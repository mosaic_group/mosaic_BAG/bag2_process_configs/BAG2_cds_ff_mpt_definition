#############################################################################
# Process specific part to be included in lvs.sh in BAG2_methods/shell/lvs.sh 
#
# Created by Marko Kosunen on 15.11.2022
#############################################################################

# Create lvs_runset.ctrl and technology.rul in LVS run directory 
CURRENTFILE=${LVS_RUN_DIR}/lvs_runset.ctrl
cat << EOF > $CURRENTFILE
text_depth -primary;
virtual_connect -colon no;
virtual_connect -semicolon_as_colon yes;
virtual_connect -noname;
virtual_connect -report yes;
virtual_connect -report_maximum all;
virtual_connect -depth -primary;
lvs_ignore_ports no;
lvs_expand_cell_on_error no;
lvs_filter_option AB AG -source_layout;
lvs_break_ambig_max 32;
lvs_abort -softchk no;
lvs_abort -supply_error no;
lvs_abort -check_device_for_property_rule no;
lvs_power_name "VDD";
lvs_ground_name "VSS";
lvs_find_shorts no;
sconnect_upper_shape_count no;
lvs_report_file "${LVS_RUN_DIR}/${CELL}.rep";
lvs_report_max 50 -mismatched_net_limit 100;
lvs_run_erc_checks yes;
lvs_report_opt -none;
report_summary -erc "${LVS_RUN_DIR}/${CELL}.sum" -replace;
max_results -erc 1000;
results_db -erc "${LVS_RUN_DIR}/${CELL}.erc_errors.ascii" -ascii;
keep_layers -none;
schematic_format cdl;
schematic_path "${LVS_RUN_DIR}/${SPICE}";
abort_on_layout_error yes;
layout_format gdsii;
layout_path "${GDS}";

EOF

CURRENTFILE=${LVS_RUN_DIR}/technology.rul
cat << EOF > $CURRENTFILE
technology "cds_ff_mpt_pvs" -ruleSet "default" -techLib "${VIRTUOSO_DIR}/pvtech.lib";
EOF

echo "Running lvs..." \
 | tee -a ${LVS_RUN_DIR}/${CELL}.lvs_run.log

pushd ${LVS_RUN_DIR} \
    && pvs \
	-lvs \
	-top_cell ${CELL} \
	-source_top_cell ${CELL} \
	-spice ${LVS_RUN_DIR}/${SPICE} \
	-control ${LVS_RUN_DIR}/lvs_runset.ctrl \
	-ui_data \
	-gdb_data \
	${LVS_RUN_DIR}/technology.rul
popd > /dev/null

sed -ne '/^.*Run Result.*$/,/^.*Extraction.*$/p' ${LVS_RUN_DIR}/${CELL}.rep.cls \
    | tee -a ${LVS_RUN_DIR}/${CELL}.lvs_run.log

## create a $CELL.success file if LVS succeeds
rm -f ${LVS_RUN_DIR}/$CELL.success
LVS_SUCCESS_FLAG=$(sed -ne '/^.*Run Result.*$/,/^.*Extraction.*$/p'  ${LVS_RUN_DIR}/${CELL}.rep.cls \
    | grep -v "MISMATCH" | grep "MATCH" )


## Process specific part ends

