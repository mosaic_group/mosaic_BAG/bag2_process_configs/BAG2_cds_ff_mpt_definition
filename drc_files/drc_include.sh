#############################################################################
# Technology dependent file that defines the rulesets and how to run the DRC 
# checks for the this technology
# To be includes in BAG2_modules/cell/drc.sh
#
# Initialized by Marko Kosunen, marko.kosunen@aalto.fi, 17.11.2022
#############################################################################

# These are the paths to the DRC ruledecks
# Different decks for BUMP/ESD, Antenna and the DRC to enable parallel runs
DECKPATH=${TECHDIR_DRC}
DRCDECK="${DECKPATH}/pvtech.lib"
ANTENNADECK=""
BUMPDECK=""

#Temporary runset file
TEMPFILE="${WORKDIR}/runset_temp"
cat << EOF > "${TEMPFILE}"
text_depth -primary;
virtual_connect -colon no;
virtual_connect -semicolon_as_colon yes;
virtual_connect -noname;
virtual_connect -report no;
virtual_connect -depth -primary;
report_summary -drc "${CELL}.sum" -replace;
max_results -drc 1000;
max_vertex -drc 4096;
keep_layers -none;
abort_on_layout_error yes;
layout_format gdsii;
layout_path "${GDS}";
EOF
#We add this line per check to the runset
#results_db -drc "${DRCDIR}/${CELL}.drc_errors.ascii" -ascii;

if [ ! -f "${WORKDIR}/runset_temp" ]; then
    echo "ERROR: No runset file created. Aborting"
    exit 1
fi

#Function for the check
run_check()
{
    CHECK="$1"
    DECK="$2"
    RUN="$3"
    DIR="$4"

    if [ "${RUN}" == '1' ] && [ ! -z "${DECK}" ]; then
        mkdir -p ${DIR} && cd ${DIR}
        echo "technology \"cds_ff_mpt_pvs\" -ruleSet \"default\" -techLib \"$DECK\";" \
            > "${DIR}/technology.rul"

        #Echo what is run
        echo "${LSFSUB} pvs -drc -top_cell ${CELL} -ui_data -control ${DIR}/drc_runset.ctrl -cell_tree ${DIR}/cell_tree.txt -dp 16 ${DECK}" && \
        $( \
            cp "${WORKDIR}/runset_temp" ./drc_runset.ctrl && \
            echo "results_db -drc \"${DIR}/${CELL}.drc_errors.ascii\" -ascii;" >> ./drc_runset.ctrl && \
            echo "Launching ${CHECK} in 20 seconds..." >> ${WORKDIR}/${CELL}_drc_log.txt && \
            sleep 20 && \
            echo "${CHECK} Running!" >> ${WORKDIR}/${CELL}_drc_log.txt && \
            ${LSFSUB} pvs -drc -top_cell ${CELL} \
                -ui_data -control ${DIR}/drc_runset.ctrl \
                -cell_tree ${DIR}/cell_tree.txt -dp 16 \
                "${DRCDIR}/technology.rul" >>  ${WORKDIR}/${CELL}_drc_log.txt 2>&1 && \
            echo -e "\nDRC run result:" >> ${WORKDIR}/${CELL}_drc_log.txt && \
            cat ${DIR}/${CELL}.sum >> ${WORKDIR}/${CELL}_drc_log.txt && \
            cd ${WORKDIR}
        ) &
        PIDLIST="${PIDLIST} $!"
    fi
}

PIDLIST=""
echo -e "Start logging to \n ${WORKDIR}/${CELL}_drc_log.txt" | tee ${CELL}_drc_log.txt

# We run Antenna and Bump checks in separate runs to speedup the execution
for CHECK in DRC ANTENNA BUMP; do
    if [ "${CHECK}" == "DRC" ]; then
        DECK="${DRCDECK}"
        RUN="${RUNDRC}"
        DIR="${DRCDIR}"
    elif [ "${CHECK}" == "ANTENNA" ]; then
        DECK="${ANTENNADECK}"
        RUN="${RUNANTENNA}"
        DIR="${ANTENNADIR}"
    elif [ "${CHECK}" == "BUMP" ]; then
        DECK="${BUMPDECK}"
        RUN="${RUNBUMP}"
        DIR="${BUMPDIR}"
    fi
    run_check ${CHECK} ${DECK} ${RUN} ${DIR}
done

