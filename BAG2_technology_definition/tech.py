# We make the mos and resistor generator methodologies local,
# as they are strongly process dependent and we want to isolate
# all process dependencies to this Module

# All the definitions are collected to BAG2_technology_definition
from . import BAG2_technology_definition


class TechInfo(BAG2_technology_definition):
    def __init__(self, process_params):
        """TechInfo class used in, yes, where actually?
           In order to be able to write this file, we
           need a documentation of what it actually does
        """
        super().__init__(process_params)
