"""BAG2 technology definition
   --------------------------
Class to defne the technology for BAG2 framework

Initially written by Marko Kosunen, Aalto University, 2019.
"""
import os
import pkg_resources
import yaml
import importlib
import copy
from typing import *

from bag.layout.template import TemplateBase
from bag.layout.tech import TechInfoConfig
from bag.layout.util import BBox

from .analog_mos.finfet import MOSTechCDSFFMPT
from .resistor.finfet import ResTechCDSFFMPTBase
from .layer_parameters import layer_parameters
from .mos_parameters import mos_parameters
from .via_parameters import via_parameters
from .resistor_parameters import resistor_parameters


class BAG2_technology_definition(TechInfoConfig):

    def __init__(self, process_params):
        TechInfoConfig.__init__(self, self.process_config, process_params)
        # These are the original settings
        process_params['layout']['mos_tech_class'] = MOSTechCDSFFMPT(self.process_config, self)
        process_params['layout']['res_tech_class'] = ResTechCDSFFMPTBase(self.process_config, self)

    @property
    def process_config(self) -> Dict[str, Any]:
        """ This is an property that parses through all the process parameters, 
        assigns them to Dictionary '_config', and returns that dict.
        """
        if not hasattr(self, '_config') or not hasattr(self, '_process_config'):
            # _yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
            # with open(_yaml_file, 'r') as content:
            #    self._process_config = yaml.load(content, Loader=yaml.FullLoader )

            # Initialize the comnpatibility Dict, originally read from Yaml file
            self._process_config = {}
            layers = layer_parameters()
            # Update the dictionary by copying the layers dict to it
            self._process_config.update(layers.property_dict)
            mos = mos_parameters()
            via = via_parameters()
            resistor = resistor_parameters()
            self._process_config['mos'] = mos.property_dict
            ## Via parameters should be isolated inside via class.
            ## These are for compatibility 
            self._process_config['via'] = via.property_dict
            self._process_config['via_name'] = via.property_dict['via_name']
            self._process_config['via_id'] = via.property_dict['via_id']
            self._process_config['resistor'] = resistor.property_dict
        self._config = self._process_config
        return self._process_config

    @property
    def grid_opts(self) -> Dict[str, Any]:
        """Dict : Global routing track definition options for the process.
        Utilized by bag_ecd.bag_desing.routing_grid.
        'num' is a placeholder for a numeric value.
        
        Keys: 
        -----
        bot_dir :  y|x , Direction fo the bottom routing layer
        layers  : list of available layer numbers
        spaces  : default spacing of the routing layers
        widths  : default widths of routing layers
        with_override: Dict of Dicts. Purpose unclear.
        """
        if not hasattr(self, '_grid_opts'):
            self._grid_opts = {'bot_dir': 'x',
                               'layers': [4, 5, 6, 7],
                               'spaces': [0.060, 0.100, 0.100, 0.100],
                               'widths': [0.060, 0.080, 0.080, 0.080],
                               'width_override': {4: {2: 0.160}, },
                               }
            return self._grid_opts
        else:
            return self._grid_opts

    @property
    def min_lch(self) -> float:
        """ Minimum lch for transistors. You may generate designs using multiples of this
        
        """
        if not hasattr(self, '_min_lch'):
            self._min_lch = 18.0E-9
            return self._min_lch
        else:
            return self._min_lch

    @property
    def config(self) -> Dict[str, Any]:
        """Contains all parameters defined in BAG2 primary configuration file 'tech_params.yaml
        """
        if not hasattr(self, '_config'):
            _yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
            with open(_yaml_file, 'r') as content:
                self._config = yaml.load(content, Loader=yaml.FullLoader)
        return self._config

    @config.setter
    def config(self, val: Dict[str, Any]):
        # This setter is here for compatibility
        # Enables redefinition of config, but it is redefined most often from the same yaml file
        self._config = val

    # Methods
    def get_transistor_primitives(self, **kwargs):
        """
        Maps given flavor and type to a name of a vendor primitive
        this can be used to enable use of vendor primitives in e.g.
        AnalogBase

        Parameters
        ----------
        flavor: str, Default 'lvt'
        type: str, Default 'nch'

        Example: self.get_trasistor_primitives(flavor='lvt', type='nch')
        """
        flavor = kwargs.get('flavor', 'lvt')
        type = kwargs.get('type', 'nch')

        # Dictionary of cell names
        primdict = {
            'nch':
                {
                    'lvt': 'nmos4_lvt',
                    'hvt': 'nmos4_hvt',
                    'standard': 'nmos4_standard',
                },
            'pch':
                {
                    'lvt': 'pmos4_lvt',
                    'hvt': 'pmos4_hvt',
                    'standard': 'pmos4_standard',
                 }
        }
        return primdict[type][flavor]

    def get_metal_em_specs(self,
                           layer_name: str,
                           w: float,
                           l: float = -1.0,
                           vertical: bool = False,
                           **kwargs: Any) -> Tuple[float, float, float]:
        """Yes, we need to document this too
        """
        metal_type = self.get_layer_type(layer_name)
        idc = self._get_metal_idc(metal_type, w, l, vertical, **kwargs)
        irms = self._get_metal_irms(layer_name, w, **kwargs)
        ipeak = float('inf')
        return idc, irms, ipeak

    # In the following methods I have replaced all values 
    def _get_metal_idc_factor(self,
                              mtype: str,
                              w: float,
                              l: float) -> float:
        """Yes, we need to document this too
        """
        # return self.get_idc_scale_factor(idc_temp, metal_type)*idc*1e-3
        return 1

    def _get_metal_idc(self,
                       metal_type: str,
                       w: float,
                       l: float,
                       vertical: bool,
                       **kwargs) -> float:
        if vertical:
            raise NotImplementedError('Vertical DC current not supported yet')

        inorm, woff = 1.0, 0.0
        idc = inorm * self._get_metal_idc_factor(metal_type, w, l) * (w - woff)
        idc_temp = kwargs.get('dc_temp', self.idc_temp)
        return self.get_idc_scale_factor(idc_temp, metal_type) * idc * 1e-3

    # noinspection PyUnusedLocal
    def _get_metal_irms(self,
                        layer_name: str,
                        w: float,
                        **kwargs) -> float:
        b = 0.0443
        k, wo, a = 6.0, 0.0, 0.2
        irms_dt = kwargs.get('rms_dt', self.irms_dt)
        irms_ma = (k * irms_dt * (w - wo) ** 2 * (w - wo + a) / (w - wo + b)) ** 0.5
        return irms_ma * 1e-3

    def get_via_em_specs(self,
                         via_name: str,
                         bm_layer: str,
                         tm_layer: str,
                         via_type: str = 'square',
                         bm_dim: Tuple[float, float] = (-1, -1),
                         tm_dim: Tuple[float, float] = (-1, -1),
                         array: bool = False,
                         **kwargs: Any) -> Tuple[float, float, float]:
        """Returns a tuple of EM current/resistance specs of the given via.

        Parameters
        ----------
        via_name : str
            the via type name.
        bm_layer : str
            the bottom layer name.
        tm_layer : str
            the top layer name.
        via_type : str
            the via type, square/vrect/hrect/etc.
        bm_dim : Tuple[float, float]
            bottom layer metal width/length in layout units.  If negative,
            disable length/width enhancement.
        tm_dim : Tuple[float, float]
            top layer metal width/length in layout units.  If negative,
            disable length/width enhancement.
        array : bool
            True if this via is in a via array.
        **kwargs :
            optional EM specs parameters.

        Returns
        -------
        idc : float
            maximum DC current per via, in Amperes.
        iac_rms : float
            maximum AC RMS current per via, in Amperes.
        iac_peak : float
            maximum AC peak current per via, in Amperes.
        """
        bm_type = self.get_layer_type(bm_layer)
        tm_type = self.get_layer_type(tm_layer)
        idc = self._get_via_idc(via_name, via_type, bm_type, tm_type, bm_dim,
                                tm_dim, array, **kwargs)
        # via do not have AC current specs
        irms = float('inf')
        ipeak = float('inf')
        return idc, irms, ipeak

    # noinspection PyUnusedLocal
    def _get_via_idc(self, vname, via_type, bm_type, tm_type,
                     bm_dim, tm_dim, array, **kwargs):
        if bm_dim[0] > 0:
            bf = self._get_metal_idc_factor(bm_type, bm_dim[0], bm_dim[1])
        else:
            bf = 1.0

        if tm_dim[0] > 0:
            tf = self._get_metal_idc_factor(tm_type, tm_dim[0], tm_dim[1])
        else:
            tf = 1.0
        factor = min(bf, tf)
        if vname == '1x' or vname == '4':
            if via_type == 'square':
                idc = 0.1 * factor
            elif via_type == 'hrect' or via_type == 'vrect':
                idc = 0.2 * factor
            else:
                # we do not support 2X square via, as it has large
                # spacing rule to square/rectangle vias.
                raise ValueError('Unsupported via type %s' % via_type)
        elif vname == '2x':
            # if via_type == 'square':
            idc = 0.4 * factor
        # else:
        #     raise ValueError('yayUnsupported via type %s' % via_type)
        elif vname == '4x':
            idc = 0.4 * factor
        elif vname == 't':
            idc = 0.4 * factor
        else:
            raise ValueError('Unsupported via name %s and bm_type %s' % (vname, bm_type))

        idc_temp = kwargs.get('dc_temp', self.idc_temp)
        return self.get_idc_scale_factor(idc_temp, bm_type) * idc * 1e-3

    def get_res_em_specs(self,
                         res_type: str,
                         w: float,
                         l: float = -1.0,
                         **kwargs: Any) -> Tuple[float, float, float]:
        """Returns a tuple of EM current/resistance specs of the given resistor.

        Parameters
        ----------
        res_type : string
            the resistor type string.
        w : float
            the width of the metal in layout units (dimension perpendicular to current flow).
        l : float
            the length of the metal in layout units (dimension parallel to current flow).
            If negative, disable length enhancement.
        **kwargs : Any
            optional EM specs parameters.

        Returns
        -------
        idc : float
            maximum DC current, in Amperes.
        iac_rms : float
            maximum AC RMS current, in Amperes.
        iac_peak : float
            maximum AC peak current, in Amperes.
        """
        idc_temp = kwargs.get('dc_temp', self.idc_temp)
        idc_scale = self.get_idc_scale_factor(idc_temp, '', is_res=True)
        idc = 1.0e-3 * w * idc_scale

        irms_dt = kwargs.get('rms_dt', self.irms_dt)
        irms = 1e-3 * (0.02 * irms_dt * w * (w + 0.5)) ** 0.5

        ipeak = 5e-3 * 2 * w
        return idc, irms, ipeak

    def add_cell_boundary(self, template: TemplateBase, box: BBox):
        """Adds a cell boundary object to the given template.

        This is usually the PR boundary.

        Parameters
        ----------
        template : TemplateBase
            the template to draw the cell boundary in.
        box : BBox
            the cell boundary bounding box.
        """
        pass

    def draw_device_blockage(self, template: TemplateBase):
        """Draw device blockage layers on the given template.

        Parameters
        ----------
        template : TemplateBase
            the template to draw the device block layers on
        """
        pass

    def get_via_arr_enc(self,
                        vname: str,
                        vtype: str,
                        mtype: str,
                        mw_unit: int,
                        is_bot: bool) -> Tuple[
                                             Optional[List[Tuple[int, int]]],
                                             Optional[Callable[[int, int], bool]]
                                         ]:
        """TODO: description

        Parameters
        ----------
        vname : string
            the via type name.
        vtype : string
            the via type, square/hrect/vrect/etc.
        mtype : string
            name of the metal layer via is connecting.  Can be either top or bottom.
        mw_unit : int
            width of the metal, in resolution units.
        is_bot : bool
            True if the given metal is the bottom metal.
        """
        return None, None
