"""
=========================
BAG2 Via parameter module 
=========================

Collection of Via related technology parameters for BAG2 framework

Initially created by Marko Kosunen, marko.kosunen@aalto.fi, 11.04.2022.

The goal of the class is to provide a master source for the Via parameters 
and enable documentation of those parameters with docstrings.

Documentation instructions
--------------------------

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

"""

import os
import pkg_resources
from typing import *
import yaml

from BAG2_technology_template.via_parameters_template import via_parameters_template
from BAG2_methods.tech.si import µm
from .layer_parameters import si_conv


class via_parameters(via_parameters_template):

    @property
    def via_units(self) -> Dict:
        """
        Property that returns a dict of via units of instances of class 'via_unit'
        NOTE: see BAG2_framework/bag/layout/core.py get_via_drc_info() return types
        """
        if not hasattr(self, '_via_units'):
            self._via_units = {}

            via_1X_square_si_dict = {
                # via horizontal/vertical spacing
                'sp': (0.042 * µm, 0.042 * µm),   # VIA1X.SP.1
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': [(0.042 * µm, 0.042 * µm)],  # VIA1X.SP.1
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3': [(0.042 * µm, 0.042 * µm)],  # VIA1X.SP.1
                # Dimensions
                'dim': (0.032 * µm, 0.032 * µm),  # VIA1X.W.1
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc': None,
                # via top enclosure rules.
                'top_enc': {   # TODO: source?
                    # via enclosure rule as function of wire width.
                    'w_list': [35, 51, 67, float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list': [
                        # list of valid horizontal/vertical enclosures.
                        [(40, 0), (0, 40)],
                        [(2, 34), (34, 2)],
                        [(28, 10), (10, 28)],
                        [(18, 18)],
                    ]
                }
            }
            via_1X_square_units_dict = si_conv.convert_si_to_unit(via_1X_square_si_dict)

            via_1X_hrect_si_dict = {
                # via horizontal/vertical spacing
                'sp': (0.042 * µm, 0.042 * µm),   # VIA1X.SP.1
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': [(0.042 * µm, 0.042 * µm)],  # VIA1X.SP.1
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3': [(0.042 * µm, 0.042 * µm)],  # VIA1X.SP.1
                # Dimensions
                'dim': (0.064 * µm, 0.032 * µm),  # VIA1X.W.2
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc': None,
                # via top enclosure rules.
                'top_enc': {   # TODO: source?
                    # via enclosure rule as function of wire width.
                    'w_list': [49, 51, 105, float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list': [
                        # list of valid horizontal/vertical enclosures.
                        [(20, 0)],
                        [(12, 9)],
                        [(10, 10)],
                        [(10, 0)],
                    ]
                }
            }
            via_1X_hrect_units_dict = si_conv.convert_si_to_unit(via_1X_hrect_si_dict)

            via_4_square_si_dict = {
                # via horizontal/vertical spacing
                'sp': (0.042 * µm, 0.042 * µm),   # TODO: source?
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': [(0.042 * µm, 0.042 * µm)],   # TODO: source?
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3': [(0.042 * µm, 0.042 * µm)],   # TODO: source?
                # Dimensions
                'dim': (0.032 * µm, 0.032 * µm),   # TODO: source?
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc': None,
                # via top enclosure rules.
                'top_enc': {   # TODO: source?
                    # via enclosure rule as function of wire width.
                    'w_list': [35, 51, 67, float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list': [
                        # list of valid horizontal/vertical enclosures.
                        [(40, 0), (0, 40)],
                        [(2, 34), (34, 2)],
                        [(28, 10), (10, 28)],
                        [(18, 18)],
                    ]
                }
            }
            via_4_square_units_dict = si_conv.convert_si_to_unit(via_4_square_si_dict)

            via_4_hrect_si_dict = {
                # via horizontal/vertical spacing
                'sp': (0.042 * µm, 0.042 * µm),   # TODO: source?
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': [(0.042 * µm, 0.042 * µm)],   # TODO: source?
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3': [(0.042 * µm, 0.042 * µm)],   # TODO: source?
                # Dimensions
                'dim': (0.064 * µm, 0.032 * µm),   # TODO: source?
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc': None,
                # via top enclosure rules.
                'top_enc': {   # TODO: source?
                    # via enclosure rule as function of wire width.
                    'w_list': [49, 51, 105, float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list': [
                        # list of valid horizontal/vertical enclosures.
                        [(20, 0)],
                        [(12, 9)],
                        [(10, 10)],
                        [(10, 0)],
                    ]
                }
            }
            via_4_hrect_units_dict = si_conv.convert_si_to_unit(via_4_hrect_si_dict)

            via_2X_square_si_dict = {
                # via horizontal/vertical spacing
                'sp': (0.062 * µm, 0.062 * µm),  # VIA2X.SP.1
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': [(0.062 * µm, 0.062 * µm)],  # VIA2X.SP.1
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3': [(0.078 * µm, 0.078 * µm)],  # VIA2X.SP.2
                # Dimensions
                'dim': (0.042 * µm, 0.042 * µm),  # VIA2X.W.1
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc': None,
                # via top enclosure rules.
                'top_enc': {   # TODO: source?
                    # via enclosure rule as function of wire width.
                    'w_list': [float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list': [
                        # list of valid horizontal/vertical enclosures.
                        [(8, 8)],
                    ]
                }
            }
            via_2X_square_units_dict = si_conv.convert_si_to_unit(via_2X_square_si_dict)

            via_4X_square_si_dict = {
                # via horizontal/vertical spacing
                'sp': (0.084 * µm, 0.084 * µm),  # VIA4X.SP.1
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': [(0.084 * µm, 0.084 * µm)],  # VIA4X.SP.1
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3': [(0.1 * µm, 0.1 * µm)],  # VIA4X.SP.2
                # Dimensions
                'dim': (0.064 * µm, 0.064 * µm),  # VIA4X.W.1
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc': None,
                # via top enclosure rules.
                'top_enc': {   # TODO: source?
                    # via enclosure rule as function of wire width.
                    'w_list': [float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list': [
                        # list of valid horizontal/vertical enclosures.
                        [(8, 8)],
                    ]
                }
            }
            via_4X_square_units_dict = si_conv.convert_si_to_unit(via_4X_square_si_dict)

            via_T_square_si_dict = {
                # via horizontal/vertical spacing
                'sp': (0.12 * µm, 0.12 * µm),  # VIAT.SP.1
                # list of valid via horizontal/vertical spacings when it's a 2x2 array
                'sp2': (0.12 * µm, 0.12 * µm),  # VIAT.SP.2
                # list of valid via horizontal/vertical spacings when it's a mxn array,
                # where min(m, n) >= 2, max(m, n) >= 3
                'sp3': (0.16 * µm, 0.16 * µm),  # VIAT.SP.2
                # Dimensions
                'dim': (0.1 * µm, 0.1 * µm),  # VIAT.W.1
                # via bottom enclosure rules.  If empty, then assume it's the same
                # as top enclosure.
                'bot_enc': None,
                # via top enclosure rules.
                'top_enc': {   # TODO: source?
                    # via enclosure rule as function of wire width.
                    'w_list': [float('Inf')],
                    # via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                    # is used if wire width is less than or equal to w_list[idx].
                    'enc_list': [
                        # list of valid horizontal/vertical enclosures.
                        [(8, 8)],
                    ]
                }
            }
            via_T_square_units_dict = si_conv.convert_si_to_unit(via_T_square_si_dict)

            # Next unit
            name = '1x'
            self._via_units[name] = via_unit(name=name)
            self._via_units[name].square = via_1X_square_units_dict
            self._via_units[name].hrect = via_1X_hrect_units_dict

            # Next unit
            name = '4'
            self._via_units[name] = via_unit(name=name)
            self._via_units[name].square = via_4_square_units_dict
            self._via_units[name].hrect = via_4_hrect_units_dict

            # Next unit        
            name = '2x'
            self._via_units[name] = via_unit(name=name)
            self._via_units[name].square = via_2X_square_units_dict

            # Next unit
            name = '4x'
            self._via_units[name] = via_unit(name=name)
            self._via_units[name].square = via_4X_square_units_dict

            # Next unit
            name = 't'
            self._via_units[name] = via_unit(name=name)
            self._via_units[name].square = via_T_square_units_dict

        return self._via_units

    @property
    def via_name(self) -> Dict[int, str]:
        """ Dictionary of via types between layer types.

        """
        return {
            0: '1x',
            1: '1x',
            2: '1x',
            3: '4',
            4: '2x',
            5: '2x',
            6: '4x',
            7: 't',
        }

    @property
    def via_id(self) -> Dict[Tuple[str, str], str]:
        """ Dictionary for mapping a via to connect of two layes to a 
        via primitive of the vendor.

        """
        return {
            (('LiPo', 'drawing'), 'M1'): 'M1_LiPo',
            (('LiAct', 'drawing'), 'M1'): 'M1_LiAct',
            ('M1', 'M2'): 'M2_M1',
            ('M2', 'M3'): 'M3_M2',
            ('M3', 'M4'): 'M4_M3',
            ('M4', 'M5'): 'M5_M4',
            ('M5', 'M6'): 'M6_M5',
            ('M6', 'M7'): 'M7_M6',
            ('M7', 'MT'): 'MT_M7',
        }

    @property
    def property_dict(self) -> Dict:
        """Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        """
        if not hasattr(self, '_property_dict'):
            # yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
            # with open(yaml_file, 'r') as content:
            #    self._process_config = {}
            #    dictionary = yaml.load(content, Loader=yaml.FullLoader )

            # self._property_dict = dictionary['via']
            self._property_dict = {}
            for key, val in vars(type(self)).items():
                if isinstance(val, property) and key != 'property_dict':
                    self._property_dict[key] = getattr(self, key)

            for key, val in self.via_units.items():
                self._property_dict[key] = val.property_dict
        return self._property_dict


class via_unit:
    """Class for via instances

    """
    def __init__(self, **kwargs):
        if kwargs.get('name'):
            self._name = kwargs.get('name')
        else:
            print('Must give name for a via instance')

    @property
    def name(self) -> Optional[str]:
        if not hasattr(self, '_name'):
            self._name = None
        return self._name

    @property
    def square(self) -> Dict[str, Any]:
        """Definition dictionary of a square via.

        Keys:
        dim : [int,int]
                Dimensions
        sp  : [ int, int]
                Via horizontal/vertical spacing
        sp2: [[int, int], [int, int]]
                List of valid via horizontal/vertical spacings when it's a 2x2 array
                Default: Nonexistent
        sp3 : [[int, int], [int, int]],
                List of valid via horizontal/vertical spacings when it's a mxn array,
                where min(m, n) >= 2, max(m, n) >= 3
                Default: Nonexistent
        bot_enc : None,
                Via bottom enclosure rules.  If empty, then assume it's the same
                as top enclosure.
                # via top enclosure rules.
        top_enc : Dict
               Via enclosure rule as function of wire width.
               Keys:
                   w_list : [int, float('Inf')]
                            Width list
                   enc_list : [
                        [[6, 6]],
                        [[6, 6]],
                        ]
                            List of list of lists of valid horizontal/vertical enclosures.
                            Via enclosure rules corresponding to each width in w_list.  enc_list[idx]
                            is used if wire width is less than or equal to w_list[idx].

        """
        if not hasattr(self, '_square'):
            self._square = None
        return self._square

    @square.setter
    def square(self, val: Dict[str, Any]):
        self._square = val

    @property
    def hrect(self) -> Dict[str, Any]:
        """ Shape definition of a horizontal rectangle
        Vertical data is derived from this.

        Default: same as the square.
        """

        if not hasattr(self, '_hrect'):
            self._hrect = self._square
        return self._hrect

    @hrect.setter
    def hrect(self, val: Dict[str, Any]):
        self._hrect = val

    @property
    def property_dict(self) -> Dict:
        """Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        """

        if not hasattr(self, '_property_dict'):
            self._property_dict = {}

            for key, val in vars(type(self)).items():
                if isinstance(val, property) and key != 'property_dict':
                    self._property_dict[key] = getattr(self, key)
        return self._property_dict
