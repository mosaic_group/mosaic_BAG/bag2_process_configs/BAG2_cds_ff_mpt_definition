"""
======
BAG2 layer definition module
======

The layer definition module of BAG2 framework. Currently collects all definition not
directly related to 

Initially created by Marko Kosunen, marko.kosunen@aalto.fi, 2020.

The goal of the class is to provide a master source for the process information. 
Currently it inherits TechInfoConfig, which in turn uses the values defined in it.
This circularity should be eventually broken.

Documentation instructions
--------------------------

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

"""
import os
import pkg_resources
from typing import *
import yaml

from BAG2_technology_template.layer_parameters_template import layer_parameters_template
from BAG2_methods.tech.si import µm, SIConverter

si_conv = SIConverter(layout_unit_in_meters=1e-6,
                      resolution_in_layout_units=0.001)

# Source for parameters: CDS_FF_MPT_DRM.pdf
# -----------------------------------------

class layer_parameters(layer_parameters_template):

    @property
    def tech_lib(self) -> str:
        """ Name of the process technology library containing transistor primitives. 
        
        """
        return 'cds_ff_mpt'

    @property
    def layout_unit(self) -> float:
        """Layout unit, in meters

        """
        return 1.0e-6

    @property
    def resolution(self) -> float:
        """Layout resolution in layout units.
        
        """
        return 0.001

    @property
    def flip_parity(self) -> bool:
        """True if this is multipatterning technology. 

        type: Boolean
        
        [TODO] Define where used and for what.
        There is a method 'use_flip_parity' for this name in 
        TechInfoConfig, so therefore we can not 
        define the property with a same name.
        
        """
        return False

    @property
    def pin_purpose(self) -> str:
        """Purpose of the pin layer used in pins constructs
            
              str : Default 'pin'

        """
        return 'pin'

    @property
    def well_layers(self) -> Dict[str, List[Tuple[str, str]]]:
        """Dictionary of list of tuples of form
        key:  (layer,purpose)
        describing the layers used for ntap and ptap.

        Current keys 
        ------------ 
                ntap : (str,str) 
                ptap : (str,str)

        """
        return {
            'ntap': [('NWell', 'drawing')],
            'ptap': []
        }

    @property
    def mos_layer_table(self) -> Dict[str, Tuple[str, str]]:
        """Dictionary of tuples of form 
        key:  (layer,purpose)
        describing the layers used for ntap and ptap.

        Current keys 
        ------------ 
                PO : (str,str), Poly layer 
                PO_sub : (str,str), Substrate Poly layer 
                PO__gate_dummy : (str,str), Dummy Poly on dummy OD
                PO_dummy : (str,str), Dummy Poly not on any OD
                PO_edge : (str,str), Edge Poly
                PO_edge_sub : (str,str), Edge Poly on substrate OD
                PO_edge_dummy : (str,str), Edge Poly on dummy OD
                PODE : None, Poly on OD edge layer

                OD : (str,str), Active layer
                OD_sub: (str,str), Substrate active layer
                OD_dummy: (str,str), Dummy active layer
                
                MP: (str,str), Gate connection metal
                MD: (str,str), OD connection metal
                MD_dummy: (str,str), dummy OD connection metal

                CPO: (str,str), cut poly
                FB: (str, str), fin boundary layer

        """
        return {
            'PO': ('Poly', 'drawing'),
            'PO_sub': ('Poly', 'drawing'),
            'PO_gate_dummy': ('Poly', 'dummy'),
            'PO_dummy': ('Poly', 'dummy'),
            'PO_edge': ('Poly', 'edge'),
            'PO_edge_sub': ('Poly', 'edge'),
            'PO_edge_dummy': ('Poly', 'edge'),
            'PODE': None,
            'OD': ('Active', 'drawing'),
            'OD_sub': ('Active', 'drawing'),
            'OD_dummy': ('Active', 'dummy'),
            'MP': ('LiPo', 'drawing'),
            'MD': ('LiAct', 'drawing'),
            'MD_dummy': ('LiAct', 'drawing'),
            'CPO': ('CutPoly', 'drawing'),
            'FB': ('FinArea', 'fin48'),
        }

    @property
    def res_layer_table(self) -> Dict[str, Tuple[str, str]]:
        """Dictionary of tuples of form 
        key:  (layer,purpose)
        describing the layers used for a metal resistors.

        Current keys 
        ------------ 
                RPDMY : (str,str), a layer drawn exactly on top of metal resistor,
                        generally used for LVS recognition purposes.

                RPO : (str,str), the "resistive poly" layer that makes a poly more resistive.
 
        """
        return {
            'RPDMY': ('RPDMY', 'drawing'),
            'RPO': ('RP0', 'drawing'),
        }

    @property
    def res_metal_layer_table(self) -> Dict[int, List[Tuple[str, str]]]:
        """ Mapping from metal layer ID to layer/purpose pair that defines
        a metal resistor.
        
        Array of tuples of form (layer,purpose)
        INdex 0 is usually (None,None) as there ins no Metal0

        """
        return {
            1: [('m1res', 'drawing')],
            2: [('m2res', 'drawing')],
            3: [('m3res', 'drawing')],
            4: [('m4res', 'drawing')],
            5: [('m5res', 'drawing')],
            6: [('m6res', 'drawing')],
            7: [('m7res', 'drawing')],
            8: [('mtres', 'drawing')],
        }

    @property
    def metal_exclude_table(self) -> Dict[int, Tuple[str, str]]:
        """Mapping from metal layer ID to layer/purpose pair that
        defines metal exclusion region.
        
        Dict of tuples of form int : (layer,purpose)


        """
        return {
            1: ('DMEXCL', 'dummy1'),
            2: ('DMEXCL', 'dummy2'),
        }

    @property
    def layer_name(self) -> Dict[Union[int, str], str]:
        """
        Mapping dictionary from metal layer ID to layer name. Assume purpose is 'drawing'.

        Dict of of form int : 'layer'
        
        """
        return {
            1: 'M1',
            2: 'M2',
            3: 'M3',
            4: 'M4',
            5: 'M5',
            6: 'M6',
            7: 'M7',
            8: 'MT',
        }

    @property
    def layer_type(self) -> Dict[str, str]:
        """Mapping from metal layer name to metal layer type.  The layer type
         is used to figure out which EM rules to use. OD has no current density
         EM rules, but layer type is used for via mapping as well. Hence, it
         is also here.

        Dict of form int : 'layer'
        
        """
        return {
            'LiPo': 'LiPo',
            'LiAct': 'LiAct',
            'M1': '1x',
            'M2': '1x',
            'M3': '1x',
            'M4': '4',
            'M5': '2x',
            'M6': '2x',
            'M7': '4x',
            'MT': 't',
        }

    @property
    def max_w(self) -> Dict[str, int]:
        """ Maximum wire width for given metal type
        
        """
        raise NotImplementedError("max_w() is not implemented in this BAG2_technology_definition!")

    @property
    def min_area(self) -> Dict[str, List[int]]:
        """ Minimum area for given metal type
        
        """
        raise NotImplementedError("min_area() is not implemented in this BAG2_technology_definition!")

    @property
    def sp_le_min(self) -> Dict[str, Dict[str, List[Union[int, float]]]]:
        """ Minimum line-end spacing rule. Space is measured parallel to wire direction

        ------
        Returns
        Dict Structure (NOTE: a,b,x,y,z ∈ int):
        {
            w_list: [a, b, inf]    # values in units
            sp_list: [x, y, z]   # values are in units
        }

        Illustration:
            widths(unit):   min_w       a         b        inf
            space(unit):            x         y       z

        NOTE: The min_w is implicit (not "part" of the w_list)

        Interpretation:
            - Space x:  valid for width interval [0, a[
            - Space y:  valid for width interval [a, b[
            - Space z:  valid for width interval [b, inf[
        """

        si_dict = {
            '1x': {        # METAL1X.SP.4.1
                'w_list':  [float('Inf')],
                'sp_list': [0.064 * µm],
            },
            '4': {         # METAL14.SP.4.1
                'w_list':  [float('Inf')],
                'sp_list': [0.064 * µm],
            },
            '2x': {        # METAL2X.SP.2.1
                'w_list':  [float('Inf')],
                'sp_list': [0.074 * µm],
            },
            '4x': {        # METAL4X.SP.4.1
                'w_list':  [float('Inf')],
                'sp_list': [0.096 * µm],
            },
            't': {         # TODO: METALT.SP.?????.????? (page 27)
                'w_list':  [float('Inf')],
                'sp_list': [0.3 * µm],
            },
        }

        unit_dict = si_conv.convert_si_to_unit(si_dict)
        return unit_dict

    @property
    def len_min(self) -> Dict[str, Dict[str, Any]]:
        """ Minimum length/minimum area rules.

        ------
        Returns
        Dict Structure:
        {
            w_list:    [a,   b,   inf] # values in units
            w_al_list: [(a1, l1), (a2, l2), (a3, l3)]    # area values are in square-units, length in units

            md_list:    [x, y, inf]
            md_al_list: [(a1, l1), (a2, l2), (a3, l3)]   # area values are in square-units, length in units
        }

        The pairs (`w_list`/`w_al_list` and `md_list`/`md_al_list`) belong together.

        ---

        The w-list is used to determine the minimum length for a given width.
        The index-correlated lists are searched through in forward direction,
        until the first w_list entry is found where given width fits.

        Illustration w-list:
            w_list(unit):     min_w        a            b            inf
            w_al_list(unit):      (a1, l1)    (a2, l2)     (a3, l3)
        NOTE: The min_w is implicit (not "part" of the w_list)

        Interpretation:
            - (a1, l1): valid area/length for width interval [min_w, a[
            - (a2, l2): valid area/length for width interval [a, b[
            - (a3, l3): valid for width interval [b, inf[

        ---

        The md-list is used to determine the maximum dimension for a given width.
        The index-correlated lists are searched through in reverse direction,
        until the first max_dim list entry is found where given width/length fits (is larger).
        """

        si_dict = {
            '1x': {
                #            METAL1X.A.1         METAL1X.W.1
                'w_list':    [float('Inf')],
                'w_al_list': [(0.006176 * µm**2, 0.032 * µm)],
                'md_list':    [float('Inf')],
                'md_al_list':  [(0.006176 * µm**2, 0.032 * µm)],   # TODO: why is METAL1X.W.2 not used?
            },
            '4': {
                #            METAL14.A.1         METAL1X.W.1
                'w_list':    [float('Inf')],
                'w_al_list': [(0.006176 * µm**2, 0.032 * µm)],
                'md_list': [float('Inf')],
                'md_al_list':  [(0.006176 * µm**2, 0.032 * µm)],   # TODO: why is METAL14.W.2 not used?
            },
            '2x': {
                #             METAL2X.A.1       METAL2X.W.1
                'w_list':     [float('Inf')],
                'w_al_list':  [(0.0082 * µm**2, 0.058 * µm)],
                'md_list':    [float('Inf')],
                'md_al_list': [(0.0082 * µm**2, 0.058 * µm)],    # TODO: why is METAL2X.W.2 not used?
            },
            '4x': {
                #             METAL4X.A.1    METAL4X.W.1
                'w_list':    [float('Inf')],
                'w_al_list': [(0.01 * µm**2, 0.069 * µm)],
                'md_list':    [float('Inf')],
                'md_al_list': [(0.01 * µm**2, 0.069 * µm)],    # TODO: why is METAL4X.W.2 not used?
            },
            't': {
                #             METALT.A.1        METALT.W.1
                'w_list':     [float('Inf')],
                'w_al_list':  [(0.0484 * µm**2, 0.22 * µm)],
                'md_list':    [float('Inf')],
                'md_al_list': [(0.0484 * µm**2, 0.22 * µm)],    # TODO: why is METALT.W.2 not used?
            },
        }

        unit_dict = si_conv.convert_si_to_unit(si_dict)
        return unit_dict

    @property
    def idc_em_scale(self) -> Dict[str, Any]:
        """Table of electromigration temperature scale factor
        """
        return {
            # scale factor for resistor
            # scale[idx] is used if temperature is less than or equal to temp[idx]
            'res': {
                'temp': [100, float('Inf')],
                'scale': [1.0, 0.5],
                # scale factor for this metal layer type
            },
            'x': {
                'temp': [100, float('Inf')],
                'scale': [1.0, 0.5],
            },
            # default scale vector
            'default': {
                'temp': [100, float('Inf')],
                'scale': [1.0, 0.5],
            }
        }

    @property
    def sp_min(self) -> Dict[str, Dict[str, List[Union[float, int]]]]:
        """ Minimum wire spacing rule.  Space is measured orthogonal to wire direction.
        Wire spacing as function of wire width.  sp_list[idx] is used if wire width is 
        less than or equal to w_list[idx].

        """
        si_dict = {
            # wire spacing as function of wire width.  sp_list[idx] is used if
            # wire width is less than or equal to w_list[idx]
            '1x': {
                #          METAL1X.SP.2.1 . METAL1X.SP.2.4
                'l_list':  [float('Inf')],
                'w_list':              [0.099 * µm, 0.749 * µm,  1.49 * µm,   float('Inf')],
                'sp_list': [0.048 * µm, 0.072 * µm, 0.112 * µm, 0.22 * µm],
            },
            '4': {
                #          METAL14.SP.2.1 - METAL14.SP.2.4
                'l_list':  [float('Inf')],
                'w_list':              [0.099 * µm, 0.749 * µm,  1.49 * µm,   float('Inf')],
                'sp_list': [0.048 * µm, 0.072 * µm, 0.112 * µm, 0.22 * µm],
            },
            '2x': {
                #          METAL2X.SP.1.1 - METAL2X.SP.1.3
                'l_list': [float('Inf')],
                'w_list':               [0.059 * µm, 0.089 * µm, float('Inf')],
                'sp_list': [0.068 * µm, 0.08 * µm,   0.1 * µm],
            },
            '4x': {
                #          METAL4X.SP.2.1 - METAL4X.SP.2.3
                'l_list': [float('Inf')],
                'w_list':              [0.499 * µm, 0.999 * µm, float('Inf')],
                'sp_list': [0.09 * µm, 0.12 * µm, 0.4 * µm],
            },
            't': {
                #          METALT.SP.1.1 - METALT.SP.1.5
                'l_list': [float('Inf')],
                'w_list':            [0.749 * µm, 1.499 * µm, 2.499 * µm, 3.499 * µm, float('Inf')],
                'sp_list': [0.2 * µm, 0.35 * µm,  0.55 * µm,  0.75 * µm,  1.25 * µm],
            }
        }

        unit_dict = si_conv.convert_si_to_unit(si_dict)
        return unit_dict

    @property
    def sp_sc_min(self) -> Dict[str, Dict[str, List[Union[float, int]]]]:
        """ Minimum wire spacing rule for same color wires (FINFET specific parameter)

        """
        return self.sp_min

    @property
    def dnw_margins(self) -> Dict[str, int]:
        """ Deep Nwell margins

        """
        return {
            'normal': 200,
            'adjacent': 400,
            'compact': 0,
        }
    #            'dnw_dnw_sp' :  int , # Deep N-well to Deep N-well with different potential space
    #            'dnw_nw_sp' :  int , # Deep N-well to N-well with different potential space
    #            'dnw_pw_sp' :  int ,
    #            'dnw_np_enc' :  int , # Minimum Deep N-well enclosure of NP
    #           }

    @property
    def implant_rules(self) -> Dict[str, Any]:
        """' Implant layer rules.

        """
        raise NotImplementedError("implant_rules() is not implemented in this BAG2_technology_definition!")

    @property
    def nw_rules(self) -> Dict[str, int]:
        """ N-well layer rules.

        """
        raise NotImplementedError("nw_rules() is not implemented in this BAG2_technology_definition!")

    @property
    def od_rules(self) -> Dict[str, Any]:
        """ OD layer rules

        """
        raise NotImplementedError("od_rules() is not implemented in this BAG2_technology_definition!")

    @property
    def property_dict(self) -> Dict[str, Any]:
        """Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        """
        if not hasattr(self, '_property_dict'):
            # yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
            # with open(yaml_file, 'r') as content:
            #  #self._process_config = {}
            #   dictionary = yaml.load(content, Loader=yaml.FullLoader )

            # self._property_dict = dictionary
            self._property_dict = {}

            for key, val in vars(type(self)).items():
                if isinstance(val, property) and key != 'property_dict':
                    try:
                        if key == 'flip_parity':
                            self._property_dict['use_flip_parity'] = getattr(self, key)
                        else:
                            self._property_dict[key] = getattr(self, key)
                    except NotImplementedError:
                        pass  # skip CMOS only parameters
        return self._property_dict
