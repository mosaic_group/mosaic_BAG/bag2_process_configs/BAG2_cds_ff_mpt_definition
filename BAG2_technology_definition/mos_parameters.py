"""
=========================
BAG2 Mos parameter module 
=========================

Collection of MOS transistor related technology parameters for BAG2 framework

Initially created by Marko Kosunen, marko.kosunen@aalto.fi, 08.04.2022.

The goal of the class is to provide a master source for the MOS parameters 
and enable documentation of those parameters with docstrings.

Documentation instructions
--------------------------

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

"""

import os
import pkg_resources
from typing import *
import yaml

from BAG2_technology_template.mos_parameters_template import mos_parameters_template
from BAG2_methods.tech.si import µm
from .layer_parameters import si_conv


class mos_parameters(mos_parameters_template):

    @property
    def mos_pitch(self) -> int:
        """OD quantization pitch.

        """
        return 48

    @property
    def analog_unit_fg(self) -> int:
        """ Number of fingers in an AnalogBase row must be multiples of this number.

        """
        return 2

    @property
    def draw_zero_extension(self) -> bool:
        """ True if zero height AnalogMosExt should be drawn.

        """
        return False

    @property
    def floating_dummy(self) -> bool:
        """ True if floating dummies are allowed.

        """
        return False

    @property
    def abut_analog_mos(self) -> bool:
        """ True if AnalogMosConn can abut each other.

        """
        return True

    @property
    def draw_sub_od(self) -> bool:
        """ True to draw OD in substrate contact.  False generally used to draw SOI transistors.

        """
        return True

    #    #MVM 2check w/ Marko
    #    @property
    def sub_ring_lch(self) -> float:
        """ channel length used in substrate rings.

        """
        return 18.0e-9

    @property
    def dum_conn_pitch(self) -> int:
        """ vertical dummy wires pitch, in routing grid tracks.

        """
        return 1

    @property
    def dum_layer(self) -> int:
        """ dummy connection layer.

        """
        return 1

    @property
    def ana_conn_layer(self) -> int:
        """ AnalogBase vertical connection layer.

        """
        return 3

    @property
    def dig_conn_layer(self) -> int:
        """ LaygoBase vertical connection layer

        """
        return 1

    @property
    def dig_top_layer(self) -> int:
        """ LaygoBase top layer.

        """
        return 2

    @property
    def imp_od_encx(self) -> int:
        """ horizontal enclosure of implant layers over OD

        """
        return 65

    @property
    def od_spy_max(self) -> int:
        """ maximum space between OD rows in resolution units.
        """

        return 600

    @property
    def od_min_density(self) -> float:
        """ minimum OD density

        """
        return 0.20

    @property
    def od_fill_w(self) -> Tuple[int, int]:
        """ dummy OD height range.

        """
        raise NotImplementedError("od_fill_w() is not implemented in this BAG2_technology_definition!")

    @property
    def md_w(self) -> int:
        """ width of bottom bound-box of OD-METAL1 via
        Defines also x width of the quard ring

        """
        return 40

    # MVM check w/M
    @property
    def od_spy(self) -> int:
        """ minimum vertical space between OD, in resolution units
        """
        return 10

    @property
    def imp_od_ency(self) -> int:
        """ implant layers vertical enclosure of active.
        this is used to figure out where to separate implant layers in extension blocks,
        """
        return 45

    @property
    def imp_po_ency(self) -> int:
        """ implant layers vertical enclosure of poly.
        this is used to figure out where to separate implant layers in extension blocks,
        if None, this rule is ignored.
        Does not seem to affect anything

        """
        raise NotImplementedError("imp_po_ency() is not implemented in this BAG2_technology_definition!")

    @property
    def nw_dnw_ovl(self) -> int:
        """ overlap between N-well layer and Deep N-well layer.

        """
        return 100

    @property
    def nw_dnw_ext(self) -> int:
        """ extension of N-well layer over Deep N-well layer.

        """
        return 100

    @property
    def min_fg_decap(self) -> Dict[str, List[Union[float, int]]]:
        """ Minimum number of fingers for decap connection.

        """
        return {
            'lch': [float('inf')],
            'val': [2]
        }

    @property
    def min_fg_sep(self) -> Dict[str, List[Union[float, int]]]:
        """ Minimum number of fingers between separate AnalogMosConn.

        """
        return {
            'lch': [float('inf')],
            'val': [2]
        }

    @property
    def edge_margin(self) -> Dict[str, List[Union[float, int]]]:
        """ space between AnalogBase implant and boundary.

        """
        return {
            'lch': [float('inf')],
            'val': [150]
        }

    @property
    def fg_gr_min(self) -> Dict[str, List[Union[float, int]]]:
        """ minimum number of fingers needed for left/right guard ring.

        """
        return {
            'lch': [float('inf')],
            'val': [2]
        }

    @property
    def fg_outer_min(self) -> Dict[str, List[Union[float, int]]]:
        """ minimum number of fingers in outer edge block.

        """
        return {
            'lch': [float('inf')],
            'val': [3]
        }

    @property
    def sd_pitch_constants(self) -> Dict[str, List[Union[float, int, List[int]]]]:
        """ source/drain pitch related constants.
        source/drain pitch is computed as val[0] + val[1] * lch_unit

        """
        return {
            'lch': [float('inf')],
            'val': [[90, 0]]
        }

    @property
    def num_sd_per_track(self) -> Dict[str, List[Union[float, int]]]:
        """ number of source/drain junction per vertical track.

        """
        return {'lch': [float('inf')],
                'val': [1]
                }

    @property
    def po_spy(self) -> Dict[str, List[Union[float, int]]]:
        """ space between PO

        """
        return {
            'lch': [float('inf')],
            'val': [0]
        }

    @property
    def mx_gd_spy(self) -> Dict[str, List[Union[float, int]]]:
        """ vertical space between gate/drain metal wires.

        """
        raise NotImplementedError("mx_gd_spy() is not implemented in this BAG2_technology_definition!")

    @property
    def od_gd_spy(self) -> Dict[str, List[Union[float, int]]]:
        """ space between gate wire and OD

        """
        raise NotImplementedError("od_gd_spy() is not implemented in this BAG2_technology_definition!")

    @property
    def po_od_exty(self) -> Dict[str, List[Union[float, int]]]:
        """ PO extension over OD

        """
        return {
            'lch': [float('inf')],
            'val': [10]
        }

    @property
    def po_od_extx_constants(self) -> Dict[str, List[Union[float, int, Tuple[int, int, int]]]]:
        """ OD horizontal extension over PO.
        value specified as a (offset, lch_scale, sd_pitch_scale) tuple,
        where the extension is computed as
        offset + lch_scale * lch_unit + sd_pitch_scale * sd_pitch_unit

        DEPRECATED: specify a constant number directly.
        (What does this mean)

        """
        return {
            'lch': [float('inf')],
            'val': [(0, 0, 1)]
        }

    @property
    def po_h_min(self) -> Dict[str, List[Union[float, int]]]:
        """minimum PO height

        """
        raise NotImplementedError("po_h_min() is not implemented in this BAG2_technology_definition!")

    @property
    def sub_m1_extx(self) -> Dict[str, List[Union[float, int]]]:
        """ distance between substrate METAL1 left edge to
        center of left-most source-drain junction.

        """
        raise NotImplementedError("sub_m1_extx() is not implemented in this BAG2_technology_definition!")

    @property
    def sub_m1_enc_le(self) -> Dict[str, List[Union[float, int]]]:
        """ substrate METAL1 via line-end enclsoure

        """
        raise NotImplementedError("sub_m1_enc_le() is not implemented in this BAG2_technology_definition!")

    @property
    def dum_m1_encx(self) -> Dict[str, List[Union[float, int]]]:
        """ dummy METAL1 horizontal enclosure.

        """
        raise NotImplementedError("dum_m1_encx() is not implemented in this BAG2_technology_definition!")

    @property
    def g_bot_layer(self) -> int:
        """ gate wire bottom layer ID

        """
        return 1

    @property
    def d_bot_layer(self) -> int:
        """ drain/source wire bottom layer ID

        """
        return 1

    @property
    def g_conn_w(self) -> Dict[str, List[Union[float, List[int]]]]:
        """ gate vertical wire width on each layer.

        """
        return {
            'lch': [float('inf')],
            'val': [[40, 40, 40]]
        }

    @property
    def d_conn_w(self) -> Dict[str, List[Union[float, List[int]]]]:
        """ drain vertical wire width on each layer.

        """
        return {
            'lch': [float('inf')],
            'val': [[40, 72, 40]]
        }

    @property
    def g_conn_dir(self) -> List[str]:
        """ gate wire directions

        """
        return ['y', 'x', 'y']

    @property
    def d_conn_dir(self) -> List[str]:
        """ drain wire directions

        """
        return ['y', 'x', 'y']

    @property
    def g_via(self) -> Dict[str, List[int]]:
        """ gate via parameters

        """
        return {
            'dim': [
                [32, 32],
                [32, 32],
                [32, 32]
            ],
            'sp': [32, 32, 42],
            # This is provided not to break old generators
            'bot_enc_le': [18, 40, 34],
            # This can be used to control gate poly extension
            #        'bot_enc_le_top' : [int , int , int ],
            #        'bot_enc_le_bot' : [int , int , int ],
            'top_enc_le': [40, 34, 40]
        }

    @property
    def prim_offset(self) -> int:
        """Y direction offset for the primitive used in Analog base

        Currently used at Aalto
           
        """
        raise NotImplementedError("prim_offset() is not implemented in this BAG2_technology_definition!")

    @property
    def d_via(self) -> Dict[str, List[int]]:
        """ drain/source via parameters

        """
        # Minimum dimension of vias from PO to mos_conn_layer -1 (2)
        return {
            'dim': [
                [32, 32],
                [32, 64],
                [32, 64]
            ],
            # Minimum spacing of vias
            'sp': [32, 42, 42],
            'bot_enc_le': [18, 20, 10],
            'top_enc_le': [40, 10, 20]
        }

    @property
    def substrate_contact(self) -> Dict[str, List[int]]:
        """Substrate contact parameter 
        Copy d_via here if needed. Used Analog Base if 
        substrate contacs differ from drain contacts

        """
        raise NotImplementedError("substrate_contact() is not implemented in this BAG2_technology_definition!")

    @property
    def dnw_layers(self) -> List[Tuple[str, str]]:
        """ Deep N-well layer names

        """
        return [
            ('dnw', 'drawing'),
        ]

    @property
    def imp_layers(self) -> Dict[str, Dict[Tuple[str, str], List[int]]]:
        """ implant layer names for each transistor/substrate tap type.

        """
        return {
            # 'nch_thick' : {
            #     ('layer', 'purpose') : [0,0],
            #     ('layer', 'purpose') : [0,0]
            # },
            'nch': {
                ('FinArea', 'fin48'): [10, 50]
            },
            'pch': {
                ('FinArea', 'fin48'): [10, 50],
                ('NWell', 'drawing'): [10, 50],
            },
            'ptap': {
                ('FinArea', 'fin48'): [10, 50]
            },
            'ntap': {
                ('FinArea', 'fin48'): [10, 50],
                ('NWell', 'drawing'): [10, 50],
            }
        }

    # MVM 2 Marko nch_thick not set - probably remove this parameter, abs class- method need to be present
    @property
    def thres_layers(self) -> Dict[str, Dict[str, Dict[Tuple[str, str], List[int]]]]:
        """ threshold layer names for each transistor/substrate tap type.

        """
        return {
            'nch': {
                'standard': {
                    ('Nsvt', 'drawing'): [10, 50]
                },
                'lvt': {
                    ('Nlvt', 'drawing'): [10, 50]
                },
                'hvt': {
                    ('Nhvt', 'drawing'): [10, 50]
                },
            },
            'pch': {
                'standard': {
                    ('Psvt', 'drawing'): [10, 50]
                },
                'lvt': {
                    ('Plvt', 'drawing'): [10, 50]
                },
                'hvt': {
                    ('Phvt', 'drawing'): [10, 50]
                },
            },
            'ptap': {
                'standard': {
                    ('Psvt', 'drawing'): [10, 50]
                },
                'lvt': {
                    ('Plvt', 'drawing'): [10, 50]
                },
                'hvt': {
                    ('Phvt', 'drawing'): [10, 50]
                },
            },
            'ntap': {
                'standard': {
                    ('Nsvt', 'drawing'): [0, 0]
                },
                'lvt': {
                    ('Nlvt', 'drawing'): [0, 0]
                },
                'hvt': {
                    ('Nhvt', 'drawing'): [0, 0]
                },
            }
        }

    @property
    def fin_h(self) -> int:
        """fin height

        """
        return 14

    @property
    def od_spx(self) -> int:
        """minimum horizontal space between OD, in resolution units

        """
        return 50

    @property
    def od_fin_exty_constants(self) -> List[int]:
        """Optional: OD vertical extension over fins

        """
        return [0, 0, 0]

    @property
    def od_fin_extx(self) -> int:
        """horizontal enclosure of fins over OD

        """
        return 216

    @property
    def no_sub_dummy(self) -> bool:
        """True if dummies cannot be drawn on substrate region

        """
        return False

    @property
    def mos_conn_modulus(self) -> int:
        """transistor/dummy connections modulus

        """
        return 1

    @property
    def od_fill_h(self) -> List[int]:
        """dummy OD height range

        """
        return [2, 20]

    @property
    def od_fill_w_max(self) -> Optional[int]:
        """dummy OD maximum width, in resolution units

        """
        return None

    @property
    def imp_min_w(self) -> int:
        """minimum implant layer width

        """
        return 52

    @property
    def imp_edge_dx(self) -> Dict[Tuple[str, str], List[int]]:
        """dictionary from implant layers to X-delta in outer edge blocks

        """
        return {
            ('pp', 'drawing'): [0, 0, 0]
        }

    # MVM check data format !!python/tuple but with dir
    #
    @property
    def mp_h_sub(self) -> int:
        """substrate MP height

        """
        return 40

    @property
    def mp_spy_sub(self) -> int:
        """substrate MP vertical space

        """
        return 34

    @property
    def mp_po_ovl_constants_sub(self) -> List[int]:
        """substrate MP extension/overlap over PO

        """
        return [16, 0]

    @property
    def mp_md_sp_sub(self) -> int:
        """substrate MP space to MD

        """
        return 13

    @property
    def mp_cpo_sp_sub(self) -> int:
        """substrate MP space to CPO

        """
        return 19

    @property
    def mp_h(self) -> int:
        """MP height

        """
        return 40

    @property
    def mp_spy(self) -> int:
        """vertical space between MP

        """
        return 34

    @property
    def mp_po_ovl_constants(self) -> List[int]:
        """ MP and PO overlap

        """
        return [16, 0]

    @property
    def mp_md_sp(self) -> int:
        """space bewteen MP and MD

        """
        return 13

    @property
    def mp_cpo_sp(self) -> int:
        """ space between MP and CPO

        """
        return 19

    @property
    def has_cpo(self) -> bool:
        """True to draw CPO

        """
        return True

    @property
    def cpo_h(self) -> Dict[str, List[Union[float, int]]]:
        """ normal CPO height

        """
        return {
            'lch': [float('inf')],
            'val': [60]
        }

    @property
    def cpo_po_extx(self) -> Dict[str, List[Union[float, int]]]:
        """ horizontal extension of CPO beyond PO

        """
        return {
            'lch': [float('inf')],
            'val': [34]
        }

    @property
    def cpo_po_ency(self) -> Dict[str, List[Union[float, int]]]:
        """ vertical enclosure of CPO on PO

        """
        return {
            'lch': [float('inf')],
            'val': [34]
        }

    @property
    def cpo_od_sp(self) -> Dict[str, List[Union[float, int]]]:
        """ CPO to OD spacing

        """
        return {
            'lch': [float('inf')],
            'val': [20]
        }

    @property
    def cpo_spy(self) -> Dict[str, List[Union[float, int]]]:
        """ CPO to CPO vertical spacing

        """
        return {
            'lch': [float('inf')],
            'val': [90]
        }

    @property
    def cpo_h_end(self) -> Dict[str, List[Union[float, int]]]:
        """ CPO height for substrate end

        """
        return {
            'lch': [float('inf')],
            'val': [60]
        }

    @property
    def md_od_exty(self) -> Dict[str, List[Union[float, int]]]:
        """ vertical extension of MD over OD

        """
        return {
            'lch': [float('inf')],
            'val': [20]
        }

    @property
    def md_spy(self) -> Dict[str, List[Union[float, int]]]:
        """ vertical space bewteen MD

        """
        return {
            'lch': [float('inf')],
            'val': [46]
        }

    @property
    def md_h_min(self) -> Dict[str, List[Union[float, int]]]:
        """ minimum height of MD

        """
        return {
            'lch': [float('inf')],
            'val': [68]
        }

    @property
    def dpo_edge_spy(self) -> int:
        """vertical space between gate PO when no CPO is used for dummy transistors
        """
        return 0

    @property
    def g_m1_dum_h(self) -> int:
        """gate M1 dummy wire height

        """
        return 40

    @property
    def ds_m2_sp(self) -> int:
        """drain/source M2 space

        """
        return 50

    @property
    def property_dict(self) -> Dict:
        """Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        """
        if not hasattr(self, '_property_dict'):
            # yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
            # with open(yaml_file, 'r') as content:
            # self._process_config = {}
            # dictionary = yaml.load(content, Loader=yaml.FullLoader )

            # self._property_dict = dictionary['mos']
            self._property_dict = {}

            for key, val in vars(type(self)).items():
                if isinstance(val, property) and key != 'property_dict':
                    try:
                        self._property_dict[key] = getattr(self, key)
                    except NotImplementedError:
                        pass  # skip CMOS only parameters
        return self._property_dict


