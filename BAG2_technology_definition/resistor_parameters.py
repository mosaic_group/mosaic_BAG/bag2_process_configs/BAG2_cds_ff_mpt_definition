"""
======
BAG2 resistor parameter module
======

The resistor parameter module of BAG2 framework.

Initially created by Marko Kosunen, marko.kosunen@aalto.fi, 12.4.2022.

Documentation instructions
--------------------------

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

"""

import os
import pkg_resources
from typing import *
import yaml

from BAG2_technology_template.resistor_parameters_template import resistor_parameters_template


class resistor_parameters(resistor_parameters_template):

    @property
    def bot_layer(self) -> int:
        """Bottom horizontal routing layer ID

        """
        return 2

    @property
    def block_pitch(self) -> Tuple[int, int]:
        """Resistor core block pitch in resolution units

        """
        return 1, 1

    @property
    def po_sp(self) -> int:
        """Space between PO and dummy PO

        """
        return 50

    @property
    def imp_od_sp(self) -> int:
        """Space between implant layer and OD.
        Used only if OD cannot be inside resistor implant.

        """
        return 10

    @property
    def po_od_sp(self) -> int:
        """Space between PO/dummy PO and dummy OD

        """
        return 10

    @property
    def po_co_enc(self) -> Tuple[int, int]:
        """PO horizontal/vertical enclosure of CONTACT

        """
        return 5, 5

    @property
    def po_rpo_ext_exact(self) -> int:
        """Exact extension of PO over RPO.  If negative, this parameter is ignored.

        """
        return -1

    @property
    def po_max_density(self) -> float:
        """Maximum PO density (recommended)

        """
        return 0.55

    @property
    def dpo_dim_min(self) -> Tuple[int, int]:
        """Dummy PO minimum width/height

        """
        return 10, 10

    @property
    def od_dim_min(self) -> Tuple[int, int]:
        """Dummy OD minimum width/height

        """
        return 10, 50

    @property
    def od_dim_max(self) -> Tuple[int, int]:
        """Dummy OD maximum width/height

        """
        return 100, 5000

    @property
    def od_sp(self) -> int:
        """Dummy OD space

        """
        return 10

    @property
    def od_min_density(self) -> float:
        """Minimum OD density

        """
        return 0.20

    @property
    def co_w(self) -> int:
        """CONTACT width

        """
        return 10

    @property
    def co_sp(self) -> int:
        """CONTACT spacing

        """
        return 10

    @property
    def m1_co_enc(self) -> Tuple[int, int]:
        """METAL1 horizontal/vertical enclosure of CONTACT

        """
        return 5, 5

    @property
    def m1_sp_max(self) -> int:
        """METAL1 fill maximum spacing

        """
        return 1000

    @property
    def m1_sp_bnd(self) -> int:
        """METAL1 fill space to boundary

        """
        return 100

    @property
    def rpo_co_sp(self) -> int:
        """Space of RPO to CONTACT

        """
        return 20

    @property
    def rpo_extx(self) -> int:
        """Extension of RPO on PO

        """
        return 50

    @property
    def edge_margin(self) -> int:
        """Margin needed on the edges

        """
        return 48

    @property
    def imp_enc(self) -> Tuple[int, int]:
        """Enclosure of implant layers in horizontal/vertical direction

        """
        return 50, 50

    @property
    def imp_layers(self) -> Dict[str, Dict[Tuple[str, str], List[int]]]:
        """Resistor implant layers list

        """
        return {
            'nch': {
            },
            'pch': {
                ('pp', 'drawing'): [10, 50],
                ('nw', 'drawing'): [10, 50],
            },
            'ptap': {
                ('pp', 'drawing'): [10, 50],
            },
            'ntap': {
                ('nw', 'drawing'): [10, 50],
            }
        }

    @property
    def res_layers(self) -> Dict[str, Dict[Tuple[str, str], List[int]]]:
        """  resistor layers list

        """

        return {
            'standard': {
                ('RES', 'drawing'): [10, 50],
            },
            'high_speed': {
                ('RES', 'drawing'): [10, 50],
                ('RES2', 'drawing'): [10, 50],
            }
        }

    @property
    def thres_layers(self) -> Dict[str, Dict[str, Dict[Tuple[str, str], List[int]]]]:
        return {
            'ptap': {
                'standard': {},
                'svt': {},
                'lvt': {
                    ('layer', 'drawing'): [0, 0],
                },
                'ulvt': {
                    ('layer', 'drawing'): [0, 0],
                },
                'fast': {
                    ('layer', 'drawing'): [0, 0],
                },
                'hvt': {
                    ('layer', 'drawing'): [0, 0],
                },
                'uhvt': {
                    ('layer', 'drawing'): [0, 0],
                }
            },
            'ntap': {
                'standard': {},
                'svt': {},
                'lvt': {
                    ('layer', 'drawing'): [0, 0],
                },
                'ulvt': {
                    ('layer', 'drawing'): [0, 0],
                },
                'fast': {
                    ('layer', 'drawing'): [0, 0],
                },
                'hvt': {
                    ('layer', 'drawing'): [0, 0],
                },
                'uhvt': {
                    ('layer', 'drawing'): [0, 0],
                },
            }
        }

    @property
    def info(self) -> Dict:
        """Resistor type information dictionary
  
        """
        return {
            'standard': {
                'rsq': 500.0,
                'min_nsq': 1.0,
                'w_bounds': (10.0, 20.0),
                'l_bounds': (20.0, 50.0),
                # True to draw RPO layer, which is a layer that makes
                # PO resistive.
                # True to draw RPDMY layer, which is a layer directly
                'need_rpo': True,
                # on top of the resistor.  Usually for LVS purposes.
                'need_rpdmy': True,
                # True if OD can be drawn in resistor implant layer.
                'od_in_res': True,
            },
        }

    @property
    def property_dict(self) -> Dict:
        """Collection dictionary of all properties of this class.
        This dictionary provides compatibility with the original BAG2 parameter calls.

        """
        if not hasattr(self, '_property_dict'):
            # yaml_file = pkg_resources.resource_filename(__name__, os.path.join('tech_params.yaml'))
            # with open(yaml_file, 'r') as content:
            # self._process_config = {}
            #   dictionary = yaml.load(content, Loader=yaml.FullLoader )

            # self._property_dict = dictionary['mos']
            self._property_dict = {}

            for key, val in vars(type(self)).items():
                if isinstance(val, property) and key != 'property_dict':
                    self._property_dict[key] = getattr(self, key)
        return self._property_dict
